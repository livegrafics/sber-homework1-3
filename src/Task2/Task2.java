package Task2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int summ = 0;
        for (int i = m; i <= n; i++) {
            summ = summ + i;
        }
        System.out.println(summ);
    }
}
