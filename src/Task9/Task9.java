package Task9;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=0;
        int count=0;
        do{
            n=scanner.nextInt();
            if (n<0){
                ++count;
            }
        }while (n<0);
        System.out.println(count);
    }
}
