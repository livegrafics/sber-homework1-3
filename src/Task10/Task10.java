package Task10;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n=scanner.nextInt();
        for (int i=0;i<n;i++){
            for (int j=0;j<n-1-i;j++){
                System.out.print(" ");
            }
            for (int j=1;j<=1+2*i;j++){
                System.out.print("#");
            }
            System.out.println();
        }
        for (int i=1;i<=n-1;i++){
            System.out.print(" ");
        }
        System.out.println("|");
        System.out.println();
    }
}
